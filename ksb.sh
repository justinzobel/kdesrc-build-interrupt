#!/bin/bash

ksb_path="~/kde/usr/bin/kdesrc-build"

function start_build {
  touch /tmp/ksbinprogress
  args="${@}"
  pfile=/tmp/ksb_$(date +%s)
  echo "${args}" > ${pfile}
  ${ksb_path} ${args} && \
  if [[ -f /tmp/ksbinprogress ]];then rm /tmp/ksbinprogress;fi && \
  rm ${pfile} 
}

if [[ -f /tmp/ksbinprogress ]]
  then
    read -p "A build is already running, end it? (yes/no) " ans
    if [[ ${ans} == "yes" ]]
      then
        pkill kdesrc-build
        # kill ${pid}
        # start new build
        resume=$(cd /tmp && ls -1 ksb_*)
        start_build ${@}
        echo "Resuming previous build"
        ${ksb_path} $(cat /tmp/${resume})
    fi
  else
    start_build ${@}
fi
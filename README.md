# kdesrc-build interrupt

This is a super simple super quick script I put together for a KDE developer who wanted to be able to interrupt a long running kdesrc-build job with a small job and then resume the existing job.

## Usage:

1. Grab `ksb.sh`
2. Make it executable (`chmod +x ksb.sh`)
3. Edit the `ksb_path` variable at the top of the file to point to your `kdersc-build` locally
4. Use it instead of the `kdersc-build` command e.g. `~/ksb.sh --include-dependencies kweather`
